import { GraphQLClient, ClientContext } from "graphql-hooks";
import Container from "./Container";
import Logo from "./Logo";
import PokemonContainer from "./components/PokemonContainer";
import ContainerSections from "./components/ContainerSections";

const client = new GraphQLClient({
  // url: process.env.REACT_APP_POKE_ENDPOINT,
  url: "https://p5k91xxvoq.sse.codesandbox.io/",
});

export default function App() {
  return (
    <ClientContext.Provider value={client}>
      <>
        <Container>
          <Logo />
          <ContainerSections />
          {/* Build your app here */}
          <PokemonContainer />
        </Container>
      </>
    </ClientContext.Provider>
  );
}
