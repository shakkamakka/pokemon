import React from 'react';
import {useState} from 'react';
import Loader from './Loader';
import Section from './Section';
import SelectBox from './SelectBox';
import ContainerStat from './ContainerStat';

const ContainerSections = () => {
  const [selectedPokemon, setSelectedPokemon] = useState();
  const onSelect = (name) => {
    setSelectedPokemon(name);
  };
  return (
    <div className="containersections">
      <Section title="Choose your Pokemon!">
        <SelectBox  onSelect={onSelect} />
      </Section>
      <Section title={selectedPokemon}>
        <ContainerStat name={selectedPokemon} />
      </Section>
    </div>
  )
}

export default ContainerSections
