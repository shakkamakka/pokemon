import React from "react";
import { useQuery } from "graphql-hooks";
import DetailPokemonSpecs from "./DetailPokemonSpecs";
import DetailPokemonMoves from "./DetailPokemonMoves";
import PropTypes from "prop-types";
import StatImage from "./StatImage";
import StatTypes from "./StatTypes";
import Loader from "./Loader";
import Stats from "./Stats";

const ContainerStat = ({ name }) => {
  const GET_POKEMON_SPECS = `query Search($name: String!) {
  Pokemon(name: $name) {
    id
    name
    image
    abilities {
      name
    }
    stats {
      name
      value
    }
    moves{
      name
      type
      learnMethod
    }
    types {
      name
    }
  }
}`;
  const { loading, error, data } = useQuery(GET_POKEMON_SPECS, {
    variables: {
      name: name,
    },
  });

  if (loading) return <Loader />;
  if (error) return "Name not found";

  return (
    <div className="containerstat">
      <section>
        <StatImage pokedata={data} />
        <Stats pokedata={data} />
        {/* <DetailPokemonSpecs data={data} /> */}
      </section>
      <section>
        <DetailPokemonMoves data={data} />
      </section>
    </div>
  );
};

ContainerStat.defaultProps = {
  name: "pikachu",
};
ContainerStat.propTypes = {
  name: PropTypes.string,
};
export default ContainerStat;
