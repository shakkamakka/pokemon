import React from 'react'

const Loader = () => {
  return (
    <div>
      <p className="double">
        <span className="ouro ouro2">
          <span className="left"><span className="anim"></span></span>
          <span className="right"><span className="anim"></span></span>
          <span className="white"></span>
        </span>
      </p>
      Loading...
    </div>
  )
}

export default Loader
