import React from "react";
import { useState } from "react";
import SelectBox from "./SelectBox";
import ContainerStat from "./ContainerStat";

const PokemonContainer = () => {
  const [selectedPokemon, setSelectedPokemon] = useState();
  const onSelect = (name) => {
    setSelectedPokemon(name);
  };
  return (
    <div className="pokemoncontainer">
      <SelectBox onSelect={onSelect} />
      <ContainerStat name={selectedPokemon} />
    </div>
  );
};

export default PokemonContainer;
