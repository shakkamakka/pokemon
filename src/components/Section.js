import React from 'react'

const Section = ({children, title}) => {
    return (
        <div className="section">
            <div className="sectionheader">{title || "pokemon"}</div>
            {children}
        </div>
    )
}

export default Section;
