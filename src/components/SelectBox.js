import React from "react";
import { useState } from "react";
import SelectBoxList from "./SelectBoxList";

// Filter Pokemons
const SelectBox = ({ onSelect }) => {
  const [pokemonFilter, setPokemonFilter] = useState("");

  return (
    <div className="selectbox">
      <input
        type="text"
        value={pokemonFilter}
        onChange={(e) => setPokemonFilter(e.target.value)}
        placeholder="Filter by name"
      />
      <SelectBoxList pokemonfilter={pokemonFilter} onSelect={onSelect} />
    </div>
  );
};

export default SelectBox;
