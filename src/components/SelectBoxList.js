import React from "react";
import { useState } from "react";
import { useQuery } from "graphql-hooks";
import PropTypes from "prop-types";
import Loader from "./Loader";

const SelectBoxList = ({ pokemonfilter, onSelect }) => {
  const [selectedId, setSelectedId] = useState();
  const GET_ALL_POKEMONS = `query Search($limit: Int) {
            Pokemons(first: $limit) {
                id
                name
            }
        }`;
  const { loading, error, data } = useQuery(GET_ALL_POKEMONS, {
    variables: {
      limit: 161,
    },
  });

  if (loading) return <Loader />;
  if (error) return "Something Bad Happened";

  const pokemonList = data
    ? data.Pokemons.filter((pokemon) => {
        return (
          pokemon.name.toLowerCase().search(pokemonfilter.toLowerCase()) !== -1
        );
      })
    : [];

  const handleClick = (id) => {
    setSelectedId(id);
  };

  return (
    <div className="list">
      <ul>
        {pokemonList.map(({ id, name }) => (
          <li
            key={id}
            onClick={() => {
              handleClick(id);
              onSelect(name);
            }}
            className={selectedId === id ? "selected" : ""}
          >
            {name}
          </li>
        ))}
      </ul>
    </div>
  );
};

SelectBoxList.defaultProps = {
  pokemonfilter: "",
};
SelectBoxList.propTypes = {
  pokemonfilter: PropTypes.string,
  onSelect: PropTypes.func,
};

export default SelectBoxList;
