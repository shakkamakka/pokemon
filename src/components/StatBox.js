
const StatBox = ({ data }) => {
  return (
        <div className="stats">
          {data.Pokemon.stats.map((stat, index) => (
            <div key={index}>
              <span className="name">{stat.name}</span>
              <span className="value">{stat.value}</span>
            </div>
          ))}
        </div>
  );
};

export default StatBox;
