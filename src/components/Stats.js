import React from 'react'

const Stats = ({ pokedata }) => {
    return (
        <div className="statscolumns">
            {pokedata.Pokemon.stats.map((stat, index) => (
            <div key={index}>
              <span className="name">{stat.name}</span>
              <span className="value">{stat.value}</span>
            </div>
          ))}
        </div>
    )
}

export default Stats
